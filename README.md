# Code Portfolio
[Online Resume](https://fabiencvonline.com)

This is just an sample of the quality work I can provide. Some repos have a separate README file to give further informations on what has been single handled or which part of a teamwork has been worked on. All sensitive informations have been removed and all unnecessary files as well. These repos are not functional but can provide some valuable informations on my skillset.

## [IGLU.net](./iglunet/)
[Archive website](https://web.archive.org/web/20180811182409/https://iglu.net/)

WordPress based site using [Sage](https://roots.io/sage/) started theme, [Toolset](https://toolset.com/home/types-manage-post-types-taxonomy-and-custom-fields/) plugin for custom posts, fields and taxonomy.

### Skills

`SCSS`, `CSS3`, `Javascript`, `jQuery`, `isotope`, `PHP`, `Wordpress`, `Photoshop`, `Illustrator`


---

*© 2019 - Fabien Crapetto, All right reserved.*