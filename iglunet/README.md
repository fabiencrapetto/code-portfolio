# IGLU.net
[Archive website](https://web.archive.org/web/20180811182409/https://iglu.net/)

WordPress based site using [Sage](https://roots.io/sage/) started theme, [Toolset](https://toolset.com/home/types-manage-post-types-taxonomy-and-custom-fields/) plugin for custom posts, fields and taxonomy.

### Skills

`SCSS`, `CSS3`, `Javascript`, `jQuery`, `isotope`, `PHP`, `Wordpress`, `Photoshop`, `Illustrator`

---

Mostly worked on the overall DOM structure, stylesheets and JS.

All the DOM structure is store in the DB as it was fully handled through Toolset Plugin.

Files that have been partially modified :
- [base.php](./base.php) (to add a main color theme per page)
- [lib/extras.php](./lib/extras.php) (body class and bigger excerpt)
- [templates/head.php](./templates/head.php) (tracking codes for SEO)
- [templates/header.php](./templates/header.php) (custom menu)

Fully handled files :
- [assets/styles/*](./assets/styles/)
- [assets/scripts/main.js](./assets/scripts/main.js)

All images and illustrations were done by me.

---

*© 2019 - Fabien Crapetto, All right reserved.*
