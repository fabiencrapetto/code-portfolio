/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
	var screen_sm_min = "768px",
		screen_md_min = "1200px";

	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.
	var Sage = {
		// All pages
		'common': {
			init: function() {
				//Adding the background on the menu element
				var nav = $('#mainNav'),
					heroPos = $('#heroGoDown').offset().top,
					menuScrollChecker = function(scroll) {
						if (scroll >= heroPos) {
							nav.addClass("menu-bg-page");
						} else {
							nav.removeClass("menu-bg-page");
						}
					};

				//Gives a background to the menu after a certain height
				$(window).on('scroll load', function() {
					var scroll = $(window).scrollTop();
					menuScrollChecker(scroll);
				});

				//Helps styling the form in the Contact section
				$('.contact-form .form-control').on("focus", function() {
					$('.contact-form.collapsed').removeClass('collapsed');
				});

				//Helps styling all forms inputs
				$('.form-control').each(function(i) {
					if ($(this).val()) {
						$(this).addClass('has-content');
					} else {
						$(this).removeClass('has-content');
					}
					$(this).on('blur', function() {
						if ($(this).val()) {
							$(this).addClass('has-content');
						} else {
							$(this).removeClass('has-content');
						}
					});
				});

				newsletter_form_focus();
				jQuery(document).bind('gform_post_render', function() {
					newsletter_form_focus();
				});

				//Click and scroll down on the arrow after Hero
				$('#heroGoDown').click(function(e) {
					var aTag = $('#afterHero'),
						offsetTag = 50;
					$('html, body').animate({
						scrollTop: aTag.offset().top - offsetTag
					}, 500);
				});

				//Adjust the color + picture for the blog posts
				if ($('.blog-header').length || $('.page-header').length) {
					var imgurl = '',
						grayscale = $('.blog-header, .page-header').data('grayscale') == true;
					if ($('.blog-header').attr('data-imgurl')) {
						imgurl = $('.blog-header').attr('data-imgurl');
					} else if ($('.page-header').attr('data-imgurl')) {
						imgurl = $('.page-header').data('imgurl');
					}
					if (imgurl !== '') {
						var style = '<style>';
						style += 'main.main .bg-main{';
						style += 'background-image: url(' + imgurl + ');';
						if (grayscale) style += 'filter: grayscale(100%);';
						style += '}';
						style += '</style>';
						$('body').append(style);
					}

					if ($('.blog-header').attr('data-cat')) {
						$('main.main').addClass($('.blog-header').data('cat'));
					}
				}

				//Adjust the color for the CV page
				if ($('body').hasClass('resource-template-default') && $('.cv-header').attr('data-dispo') !== undefined) {
					var dispo = $('.cv-header').data('dispo') ? 'available' : 'available-soon';
					$('body').addClass(dispo);
				}

				//Click event on the contact buttons and the FAQ menu button
				$('a[href="#contact"], a[href="#learn_more"]').click(function(e) {
					e.preventDefault();
					var aid = $(this).attr('href'),
						aTag = $(aid);
					$('#contact.visible,#learn_more.visible').removeClass('visible');
					aTag.toggleClass('visible');
					if (aTag.hasClass('visible')) {
						$('body').addClass('noscroll');
						$('#bgHidder').addClass('visible');
					} else {
						$('body.noscroll').removeClass('noscroll');
						$('#bgHidder').removeClass('visible');
					}
					$('#mainNavbarCollapse.in').removeClass('in');
				});

				//Click events to close contact/FAQ panels
				$('.btn-close, #bgHidder').click(function(e) {
					$('#contact.visible, #learn_more.visible, #bgHidder').removeClass('visible');
					$('body.noscroll').removeClass('noscroll');
				});

				//Looks for key pressed
				$(document).keyup(function(e) {
					if (e.keyCode == 27 && $('#bgHidder').hasClass('visible')) {
						$('#contact.visible, #learn_more.visible, #bgHidder').removeClass('visible');
						$('body.noscroll').removeClass('noscroll');
					}
				});

				//Override the toc plugin
				if ($('#toc_container').length) {
					eraseCookie('tocplus_hidetoc');
					$('#toc_container').addClass('contracted');
					$('#toc_container ul.toc_list').hide('fast');
					$('#toc_container .toc_toggle a').text('show');
				}

				//Contact form (relocate)
				$('.contact-interest').on('change', function() {
					var sel = $(this).find(":selected").val();
					if (sel == 'relocate') $('.relocate-choices').addClass('chosen');
					else $('.relocate-choices').removeClass('chosen');
				});


				//CV (Resource)
				if ($('span[data-lvl]').length) {
					$('span[data-lvl]').each(function() {
						var lvl = $(this).data('lvl'),
							el = $(this).children();
						for (var i = 0; i < (lvl - 1); i++) {
							el.clone().appendTo(this);
						}
					});
				}

				//Avoid the scrolling event to slow the scroll of a page (iframe must be in a div.map-container)
				$('.map-container').click(function() {
					$(this).find('iframe').css("pointer-events", "auto");
				});

				$(".map-container").mouseleave(function() {
					$(this).find('iframe').css("pointer-events", "none");
				});

			},
			finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
			}
		},
		// Home page
		'home': {
			init: function() {},
			finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
			}
		},
		// About us page, note the change from about-us to about_us.
		'join': {
			finalize: function() {
				// JavaScript to be fired on the about us page
				// fbq('track', 'Lead');
				// console.log('join', 'fbq - lead');
			}
		},
		// About us page, note the change from about-us to about_us.
		'hire': {
			finalize: function() {
				// JavaScript to be fired on the about us page
				// fbq('track', 'Lead');
				// console.log('hire', 'fbq - lead');
			}
		},
		'blog': {
			init: function() {
				// JavaScript to be fired on the blog page
				var $grid = $('.blog-container'),
					filterMe = function(grid, cat) {
						var filtering = '.cat-' + cat;
						if (cat == "all-categories") {
							filtering = '*';
						}
						grid.isotope({
							filter: filtering
						});
					},
					column_class = '.col-xs-12';
				if (window.matchMedia('screen and (min-width:' + screen_sm_min + ')').matches) {
					column_class = '.col-sm-6';
				}
				if (window.matchMedia('screen and (min-width:' + screen_md_min + ')').matches) {
					column_class = '.col-md-4';
				}
				var grid_options = {
					itemSelector: '.blog-tile',
					percentPosition: true,
					originTop: true,
					layoutMode: 'masonry',
					masonry: {
						columnWidth: column_class
					}
				};
				$grid.isotope(grid_options);
				$('.blog-cat').click(function() {
					var cat = $(this).attr('href').substring(1);
					filterMe($grid, cat);
					console.log(column_class);
				});
				if (window.location.href.indexOf("#") > -1) {
					var url = window.location.href,
						cat = url.substring(url.indexOf('#') + 1);
					if (cat != '') {
						filterMe($grid, cat);
					}
				}
			}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.


function newsletter_form_focus() {
	var $label = jQuery('.newsletter-form label'),
		$btn = jQuery('.newsletter-form input[type="submit"]');
	jQuery('.newsletter-form input[type="email"]').focus(function() {
		$label.addClass('hid');
		$btn.addClass('bg-pcolor');
	}).blur(function() {
		if (jQuery(this).val()) {
			jQuery(this).addClass('has-content');
		} else {
			$label.removeClass('hid');
			$btn.removeClass('bg-pcolor');
			jQuery(this).removeClass('has-content');
		}
	});
}

function setCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";
}

function eraseCookie(name) {
	setCookie(name, "", -1);
}