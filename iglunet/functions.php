<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function list_menu($atts, $content = null) {
  extract(shortcode_atts(array(  
    'menu'            => '', 
    'container'       => 'div', 
    'container_class' => '', 
    'container_id'    => '', 
    'menu_class'      => '', 
    'menu_id'         => '',
    'echo'            => true,
    'fallback_cb'     => 'wp_page_menu',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'depth'           => 0,
    'walker'          => '',
    'theme_location'  => ''), 
    $atts));
 
 
  return wp_nav_menu( array( 
    'menu'            => $menu, 
    'container'       => $container, 
    'container_class' => $container_class, 
    'container_id'    => $container_id, 
    'menu_class'      => $menu_class, 
    'menu_id'         => $menu_id,
    'echo'            => false,
    'fallback_cb'     => $fallback_cb,
    'before'          => $before,
    'after'           => $after,
    'link_before'     => $link_before,
    'link_after'      => $link_after,
    'depth'           => $depth,
    'walker'          => $walker,
    'theme_location'  => $theme_location));
}
//Create the shortcode
add_shortcode("listmenu", "list_menu");
